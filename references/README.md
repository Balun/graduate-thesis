# Prolaženje literature za diplomski rad

U ovom dokumentu naaz se bilješke sve literature koja se koristila za istraživanje i implementaciju
ovog rada. Sve reference su korektno referencirane u samome radu preko alata bibtex. Sva korištena
literatura je legalna i javno dostupna na internetu. Spomenuta literatura nalazi se kompletno unutar
ovog direktorija.

## 1. Stanford CS231 - Lecture 12: Detection and Segmentation
Ova prezentacija prati 12. predavanje s kolegija _Convolutional neural networks for visual
recognition_ sa studija računarstva sveučilišta Stanford. Spomentuo predavanje nudi jako elegantan
presjek svih problema i zadaća detekcije, semantičke segmentacije, klasifikacije, vodeći
tako do kompletnog probelma koji u pravilu ovaj rad i proučava i nastoji rješiti. Sama prezentacija
ne ulazi previš€ u dubinu pojedinih problematika, neđutim opisuje osnovne modele i principe kojima se 
spomenute zadaće rješavaju. Uvode se pojmovi downsamplinga i upsamplinga za arhitekturu konvolucijske 
mreže za segmentaciju (U-net), kao i naprednije arhitekture poput RCNN, Fast-RCNN, Faster-RCNN
te na poslijetku i __Mask RCNN__, čija je demistifikacija i upotreba idejni cilj ovoga rada.
Elegantan presjek svih spomenutih zadaća, vodeći od jednostavnijeg problema prema težemu
može se vidjeti na sljedećoj slici:

![presjek segmentacijskih zadaća](img/1_1.png)

- __Klasifikacija__ - Želimo odrediti jednu od N kategorija objekta koji se nalazi na slici. Uobičajeno rješavanje 
klasičnim konvolucijskim klasifikacijskim modelima.
- __Semantička segmentacija__ - Pridjeljujemo kategoriju svakom pikselu, kako bismo odredili interesna
područja na slici. Uobičajeno rješavanje downsampling-upsampling modelima poput U-Net-a.
- __Detekcija objekata__ - Pridjeljujemo okvire više različitih mogućih objekata, te ih klasificiramo u više mogućih kategorija.
Rješavanje pomoću RCNN-a (nadogradnje Fast RCNN i Faster RCNN).
- __Segmentacija instanci__ - Obavljamo istu zadaću kao i iznad, samo što uz pridjeljenje okvire
dajemo pikselima objekata njihove segmentacijske maske, kako bismo znali i interesna područja objekata na slici i njihove 
kategorije. Uobičajeno rješavanje s Mask RCNN.

## 2. Rich feature hierarchies for accurate object detection and semantic segmentation
Ovaj članak bavi se detekcijom objekata, testirano na PASCAL VOC datasetu, koji se može naći opisan
također u ovome direktoriju. Ovo je već stariji članak (listopad 2014.), te je ovdje pručavan
u svrhu istraživanja strukture RCNN i njenog napretka u kompleksnosti i tučnosti rješavanja
problema. izvorni kod dostupan je [ovdje](http://www.cs.berkeley.edu/~rbg/rcnn). 

### 2.1 Uvod
Malo priče o povijesit računalnog vida i hijerarhijskih modela koji se se prije koristili. 2012.
Konvolucijski modelu postižu prve značajno bolje rezultate na ImageNet skupu za klasifikaciju.
Postavlja se pitanje u kojoj mjeri se ti rezultati mgu generalizirati na PASCAL VOC, skup
za detekciju objekata. U nekom trenutku se problem detekcije rješavao klizećim prozorim na koji
se veže konvolucijska mreža, međutim ispstavlja se da za dovoljno dobru preciznot spomenuti konvolucijski
model treba imat veći broj parametara, te stoga cijal priča postaje puno memorijski i procesno
skuplja i složenija. Iz tog razloga, prvo se korisit algoritam za predlaganje interesnih regija
koji se ne uči, te se iz tih regija onda tek provodi detekcija/klasifikacija s dubokim modelom, optilike na 2000
predloženih regija. Iz tih regija izvlače se značajke konvolucijskom mrežom, te se trojem potpornih
vektora (SVM) obavlja klasifikacija za svaku regiju. R-CNN dolazi od _Regions with CNN features_.
Srednja točnost ovog modela (mAP) je 54%. 

### 2.2 Object detection with R-CNN
#### 2.2.1. Dizajn modela
- Za predlaganje interesnih regija bez kategorijskih značajki koristi se metoda 
_[Selective search](https://www.researchgate.net/publication/262270555_Selective_Search_for_Object_Recognition)_.
- Za izvlačenje značajki u svakom predloženom području koristi se arhitektura opisana u 
[ovome članku](https://papers.nips.cc/paper/4824-imagenet-classification-with-deep-convolutional-neural-networks.pdf).
Izvlači se vektor značajki s 4096 dimenzija, a značajke su izračunate unaprijednom propagacijom 
i normalizacijom 227x227 RGB slike preko 5 konvolucijskih slojeva i dva potpuno povezana sloja.

#### 2.2.2. Test-time detection
Prilikom testiranja obavlja se selektivna pretraga kako bi se izvuklo 2000 interesnih regija.
Svaka regija prilagodi se u fiksnu kvadratnu veličinu, propagira kroz konvolucijsku mrežu, te se
dobivene značajke klasificiraju strojeva potpornih vektora.

![konvolucijska mreža](img/2_1.png)

Ono što čini detekciju efikasnom je to što se parametri konvolucijske mreže dijele za sve klase.
Još jedna dobra stvar je to što su vektori značajki relativno malih dimenzija. Tako jedine 
kategorijsko-specifične komputacije ostaju skalarni produkti između značajki i težina SVM-a.

#### 2.2.3. Učenje
Konvolucijski model je pred-treniran na velikom datasetu _ILSVRC2012_. Fino podešavanje 
parametara (_engl. fine-tuning_) obavljano je kako bi se mreža prilagodila za zadaću detekcije
na iskrivljenim predloženim regijama, nastavlja se učenje stohastičkim gradijentnim spustom na
spomenutim regijama. Početna stopa učenja je 0.001. 

Što se tiče klasifikacije, nužno je bilo odrediti stopu u kojoj se mjeri promatra parcijalno
preklapanje površine objekta koji pripada nekoj klasi. Ta stopa je dobivena eksperimentalno
(_Grid search_).

#### 2.2.4. Rezultati na skupu PASCAL VOC 2010-12
![VOC rezultati](img/2_2.png)

#### 2.2.5 Rezultati na skupu ILSVRC2013
![ILSVRC2013 rezultati](img/2_3.png)

### 2.5. Semantička segmentacija
Klasifikacija regija je standadna tehnika zasemantičku segmentacju, što omogućuje korištenje ovog
modela u te svrhe. 

![rezultati segmentacije](img/2_4.png)

![rezultati VOC 2011](img/2_5.png)

### 2.7. Appendix C: Bounding-box regression
Nakon vrednovanja svake preporučene regije interesa pomoću SVM klasifikatora, Izračunava se
novi kvadrat za detekciju pomoću regresora specifičnih za svaku klasu. 

### 2.8. Primjeri
![primjeri](img/2_6.png)

## 3. Fast RCNN
Ovaj članak prestavlja model brze RCNN mreže iliti Fast R-CNN. Temeljeno na radovima na R-CNN-u,
ovaj model rješava klasifikacijske (detekcijske) probleme s poboljšanjem brzine treniranja i testiranja. Na 
primjer, prilikom treniranja modela koristeći VGG-16 bazni model, Fast RCNN brži je 9 puta, a
prilikom testiranja 213 puta, te postiže veću srednju preciznost na PASCAL VOC datasetu nego 
prijašnji model. Izvorni kod dostupan je [ovdje](https://github.com/rbgirshick/fast-rcnn)

### 3.1 Uvod.
Malo osvrta na prije spomenuti klasični RCNN i SSP mrežu. Spomenuti su veoma jasni nedostaci tog
modela: Kompicirana cjevovodna arhitektura, veoma skup trening u vremenu i memorijskom prostoru, 
kao i spor rad prilikom testiranja (47 sekundi po fotografiji za bazni model VGG16, turbo sporo).
Ponuđeno rješenje FAS RCNN nudi slijedeće beneficije:
- Bolju kvalitetu detekcije
- trenirenje u jednoj razini, koristeći gubitak za više zadataka (multi-task loss)
- treniranje može osvježiti sve slojeve mreže (u prijašnjem rješenju su neki bili fiksni)
- Nije potrebno korištenje prostora s diska za caching

### 3.2 FAST R-CNN arhitektura i postupak učenja
![Fast R-CNN](img/3_1.png)

Na gornjoj slici prikazana je arhitektura Fast R-CN modela. Ulaz u mrežu je cijela slika te set
predloženih objekata. Mreža obrađuje cijelu sliku s nekoliko konvolucijskih slojeva, te max-pooling
slojeeva kako bi se proizvela konvolucijska mapa značajki. Tada, za svaki predložak objekta RoI
(_Region of interest_) pooling sloj vadi vektor značajki fiksne duljine iz mape značajki. Svaki
vektor značajki prolazi kroz kroz niz potpuno povezanih slojeva koji se zatim križaju u dva
izlazna sloja: Jedan koji koji predviđa K softmaz vjerojastnosti za K razreda + jednu za takozvani
objekt pozadine (_background. object_). Drugi izlazni sloj predviđa 4 decimalna broja za svaku od
K klasa. Svaka ta četvorka predstavlja okvir i poziciju za klasu na slici. Ogromna prednost ovog
pristupa leži u mogućnosti unaprijednog prolaza cijele mreže odjednom, kao i backprop unazadnog prolaza, 
tog se kod prijašnjeg modela moralo raditi tri odvojena prolaza u svakoj iteraciji učenja. Značajka
koja to najviše opisuje je gbitak za više zadaća odnosno _Multi-task loss_.

![Multi-task](img/3_2.png)

Višestruki gubitak prilikom učenja kombinira gubitak klasifikacije i egresije pozicija i dimenzija okvira
objekata. Za detkciju okvira regularizirani gubitak izgleda ovako:

![regresija](img/3_3.png)

Hiperparametar lamda kontrolira odnos između utjecaja gubitaka jedne i druge zadaće u ukupnom
gubitku. U eksperimentiram je taj parametar postavljen na 1. Sam RoI pooling sloj koji je specifičan 
za ovaj model koristići slijedeću formulu za parcijalnu derivaciju prilikom unazadnog prolaza:

![RoI](img/3_4.png)

### 3.3. Rezultati
Tri glavna rezultata predstavljaju doprinose ovog članka:
- Vrhunska preciznost na skupu PASCAL VOC07
- brzo vrijeme učenja i testiranja u odnsu na R-CNN i SPPnet
- fino podešavanje konvolucijskih slojeva u VGG-16 povećava preciznost

![rezultati](img/3_5.png)

## 4. Faster R-CNN
Izvorni kod ovog članka dostupan je na 
[ovoj poveznici](https://github.com/rbgirshick/py-faster-rcnn). 

### 4.1. Uvod
U trenutku kda je izašao ovaj članak, metode koje su koristile mreže za prijedlog područja
(_Region proposal networks, R-CNN_) davale se u fenomenalne real-time rezultate za detekciju
objekata, posebice _Fast R-CNN_, ne uzimajući
u obzir vrijeme utrošeno u predlaganje područja (_region proposal_). Upravo to vrijeme zapravo
je usko grlo u testiranju dtekcije svih arhitektura, te je ideja ovog članka minimizirati čak i 
to vrijeme. Ovo je prvi članak u kojem se duboka konvolucijska mreža korsti za predlaganje regija,
i to tako da dijeli konvolucijske značajke s mrežama za detekciju vsokih performansi, dakle
radi se o novim mrežama za prijedlog regija, _Region Proposal Networks, (RPN)._ Slikoviti prikaz
različitih pristupa predlaganja područja prikazan je u nastavku, a ovdje je posebice interesantan
princip c).

![FRegion proposals](img/4_1.png)

### 4.2 Model
![Faster R-CNN](img/4_2.png)

Na slici iznad prikazan je unificirani model koji se sastoji od dvija modula: Prvi je duboka
konvolucijska mreža koja predlaže regije, a drugi je Fast R-CNN (proučen u prethodnom poglavlju)
koji koristi predložene regije. Koristeći mehanizam "pažnje" (engl. _attention_) proučavan u 
neuronskim mrežama, prvi modul govori drugom gdje treba tražiti objekte za detekciju. U sljedeća
dva podpoglavlja objašnjene su obje komponete unificiranog modela.

#### 4.2.1 Region proposal networks
Prvi modul tj. mreža za predlaganje regija uzima kao ulaz sliku proizvoljnih dimenzija te kao
izlaz daje skup prijedloga objekata (_object proposals_), svaki prijedlog sa svojstvenom karakterisitkom
objektivnosti (_objectness score_). Ideja cijele priče je da ova mreža i Fast R-CNN detektor iz
drugog modula dijele zajedničk skup konvolucijskih slojeva. NA izlazima iz konvolucijskih
slojeva koristi se "mala" mreža koja u dugu mehanizma klizećeg prozora (_sliding window_) 
generira predložene regije. 

Za svaki prijedlog regije koristi se mehanizam takozvanih "sidra", čija je poana da se pokriju
različite dimenzije visine i širine mogućih prijedloga objekata unutar tog jednog klizećeg segmenta.
Broj tih sidra je proizvljan, u ovom istraživenju korišteno je k=9 za svaki klizeći segment. 
Grafički prikaz ispod bi mogao malo razbistriti situaciju. Bitno svojstvo ovih sidra je translacijska
invarijatnost, za razliku od nekih drugih sličnih mehanizama.

![anchor](img/4_3.png)

Što se tiče treniranja mreža za predlaganje regija, pridodaje se binarna labela za svako sidro,
što predstavlja dali nešto je objekt ili nije. Pozitivna klasa daje se onim sidrima koja imaju 
najveće preklapanje preko unije (_ntersection over union, IoU_) u odnosu na prave okvire iz skupa
za učenje, ili onim sidrima koja imaju IoU veći od 0.7 sa bilo koji okvirom iz skupa za učenje.
Negativna labela daje se onim sdrima čiji je IoU mani od 0,3 za sve okvire iz skupa za učenje. 
Sidra kojima nije dana ni jedna ni druga labela ne utječu na proces učenja. 

Sa gore danim definicijama, minimizira se funkcija gubitka za ovaj modul (Multi-task loss) koja
je definirana kao kombinacija klasifikacijskog i regresijskog gubitka na sljedeći način:

![loss](img/4_4.png)

Klasifikacijski gubitak predstavlja binarnu klasifikaciju dali je sidro objekt ili nije, dok je
funkcija za regeresijski gubitak definirana kao u članku o Fast-RCNN. Za regresiju okvira uzeta
je parametrizacija iz orginalnog članka za R-CNN:

![reg_loss](img/4_5.png)

Trening RPN-a je odrađen stohastičkim gradijentnim spustom pomoću backpropagation-a.

#### 4.2.2. Dijeljenje značajki RPN-a  Fast R-CNN-a
Model je u eksperimentima ovog članka nauče takozvanim __alterniranim učenjem u 4 koraka__ 
(_4-step alternation training_). Alternirajuće učenje znači da se prvo nauči RPN, zatim
se koiste predlošci regija kako bi se načio Fast-RCNN. Mreža prilagođena od strane Fast R-Cnn-a
zatim je korištena za incijalizaciju RPN-a, i tako iterirajući. Četiri koraka u ovome algoritmu
idu ovako:
- Prvo se nauči RPN model kao što je opisano u prethodnom podpoglavlju. Taj model je inicijaliziran
s ImageNet predtreniranim modelom, te podešen i naučen do kraja za zadaću predlaganja područja
- U dugom koraku trenira se odvojena mreža za detekciju kao Fast R-CNN, koristeći predloške iz
koraka 1.Ta mreža također je inicijalizirana s ImageNet predtreniranim modelom. U ovome trenutku
dvije komponente modela ne dijele zajedničke konvolucijske parametre. 
- U trećemu koraku koristi se mreža za detekciju kao inicijalizacija RPN učenja, međutim konvolucijski
slojevi koje planiramo dijeliti se fiksiraju te se osvježavaju samo slojevi specifični za RPN. Sada
mreže dijele slojeve
- Konačno, držeći sajedničke konvolucijske slojeve fiksnima, podešavaju se slojevi specifični za
Fast R-CNN. 

ovaj višekomponentni trening može se izvesti više puta, međutim eksperimenti ne pokazuju pre velike
napretke u rezultatima u odnosu na jedan sveukupni prolaz. 

### 4.3 Rezultati
![rez1](img/4_6.png)

![rez2](img/4_7.png)

![rez3](img/4_8.png)

![rez4](img/4_9.png)

![rez5](img/4_10.png)

![rez6](img/4_11.png)

## 5. Mask R-CNN
Izvorni kod ovog članka dostupan je 
[ovdje](https://github.com/facebookresearch/Detectron). Ovo je dakle prvi članak koji daje potpuno
rješenje problema segmentacije instanci pomoću svih spomenutih mehanizama u prošlim poglavljima.
Krenuvši od predikcije interesnih regija (_Region of interest, RoI_), preko detekcije objekata
tj. klasifikacije objeata u okvirima interesnih područja (Faster R-CNN), Mask R-CNN dodaje još
jednu komponentu Faster R-CNN arhitekturi koja za svako detektirani/klasificirani objekt na slici
daje i njegovu segmentnu masku, rješavajući tako kompletan problem segmentacije instanci. Iako zbog
dodane funkcionalnosti segmentacije model radi nešto sporije nego jednostavniji Faster R-CNN, i dalje
se držimo u okvirima segmentacije instanci u stavrnom vremenu od 5 fps-a koristeći GPU, dobivajući tako
preformanse ekvivalentne Fast R-CNN koji rješava samo problem detekcije. Dodavajući manju potpuno
kovolucijsku mrežu (_Fully convolutional network, FCN_) na Faster R-CNN nemahizam za svako interesnu regiju,
kao i _RoIAlign_ sloj o kojem će biti riječi kasnije, implementacija se svodi na jednostavnu nadogradnju
Faster R-CNN mehanizma, pod predpostavkom da je on dan i adekvatan. U sljedeće dvije slike može se vidjeti
konceptualna arhitektura Mask R-CNN modela, kao i njegov izlaz na klasičnom COCO skupu podataka.

![mask rcnn](img/5_1.png)

![mask rcnn coco](img/5_2.png)

### 5.1 Model
U ideji je model konceptualno jednostavan. Faster R-CNN arhitektura daje dva izlaza za svakog kandidata
za objekta: labelu razreda i okvir objekta. Mask R-CNN u suštini dodaje  treći izlaz koji daje segmentnu masku
za svaki takav detektirani objekt. Problem leži u tome što je potrebno puno finije određivanje tih
segmenata/objekata, te se uvodi ključan element ove arhitekture, a to je pixel na pixel poravnjanje
(_pixel-to-picel alignment_), što je glavna stvar koja fali Fast/Faster R-CNN arhitekturi za izvršavanje
ove zadaće. 

Tijekom učenja, zbog trostruke zadaće ovog modela definira se višezadaćni gubitak (_multi-task loss_)
koji izgleda ovako:

![multi-task loss](img/5_3.png)

Treći gubitak je upravo gubitak segmentacije tj. segmentacijske maske, koja je definirana binarno po pixelu
za svaki RoI dan od već prethodno sloja modela. Na tu mqsku primjenjuje se sigmoida po pixelu, te se 
gubitak maskiranje (Lmask) definirao kao prosječni binarni gubitak unakrsne entropije. Nužno je spomenuti
da se maske za pojedine razrede ne natječu prilikom određivanja izlaza.

Svaka maska koja se predviđa za svaki _RoI_ dolazi iz manje potpuno konvolucijske mreže (_FCN_). 
Raid takvog rješenja problema proizlazi gradnja _RoIAllign_ loja, koji igra ključnu ulogu u predikciji
maski. 

#### 5.1.1. RoI Allign
Postojeći mehanizam _RoIPool_ iz prijašnjih modela pravi probleme jer se prilikom sažimanja gube
translacijske značajke objetaka na slici, što nije neki preveliki problem za klasifikaciju tj detekciju
objekata, međtuim za finu segmentaciju je ogroman. Ovdje se koristi bilinearna interpolacija kako bi se 
izračunale stvarne vrijendosti ulaznih značajki u 4 uzorkovana područja za svaki RoI skup. 

#### 5.1.2. Arhitektura

![arch](img/5_4.png)

### 5.2. Rezultati

![res1](img/5_5.png)

![res2](img/5_6.png)

![res3](img/5_7.png)

![res4](img/5_8.png)

#### 5.2.1 Detekcija ljudske poze
Pokazuje se da su minimalne promjene na modelu potrebne kako bi se Mask R-CNN arhitektura primjenila
na problematiku estimacije poze. Potrebno je napraviti promjenu na sustavu za segmentaciju. Za svaki od
K (broj koji je unaprijed određen kao broj "zglobova"), cilj učenja je _one-hot_ binarna maska u kojoj
je samo jedan pixel označen u prvome planu. Tijekom učenja, za svakio vidljivi keypoint (zglob) iz skupa
za učenje minimiziramo gubitak unakrsne entropije softmax izlaza, što rezultira detekcijom jedne ključne
točke. Zglobovi se tretiraju međusobno neovisno. 

![res5](img/5_9.png)

### 5.4. Dodatak - Eksperimentiranje na skupu urbanih površina
Pokazuje se da se ova arhitektura može također primjeniti na rješavanje problema segmentacije instanci
u urbanim područjima. Primjer takvog skupa podataka je 
[The Cityscapes Dataset](https://www.cityscapes-dataset.com/). Koristeći ResNet-FPN-50 kao bazni model,
arhitektura daje bolje rezultate nego bilo koji tadašnji model testiran na tim podacima. Radi dodatnog 
povećanja preciznosti, obavljen je i eksperiment sa predtreniranim modelom na COCO skupu, koji je onda
kasnije fino prilagođen za ovaj dataset, te su tada rezultati još bolji. To nam govori o važnosti
količine podataka i mogućnosti kompenzacije manjka, s obzirom da ovaj eksperiment nije obavljen na 
velikoj količini podataka iz skupa urbanih površina.

![urban](img/5_10.png)

![urban-results](img/5_11.png)
