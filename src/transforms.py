"""
"""
import cityscapesscripts.helpers.labels as labels

import numpy as np
import torch
from torchvision.transforms import functional as F
from torchvision.transforms import Resize

from PIL import Image

import random


class SimplifyAnnotation():

    def __call__(self, image, target):
        label_dict = labels.id2label

        def simplify(x):
            if x > 1000:
                x_cat = label_dict[int(np.floor(x / 1000))].categoryId
                x_inst = x % 1000
                return x_cat * 1000 + x_inst

            return label_dict[x].categoryId

        transformation = lambda x: simplify(x)
        vfunc = np.vectorize(transformation)
        simple_anot = vfunc(target)

        return image, simple_anot


class AnnotationToMaskRCN():

    def __call__(self, image, target):
        obj_ids = np.unique(target)
        obj_ids = obj_ids[1:]  # remove class zero

        masks = target == obj_ids[:, None, None]

        boxes = []
        for i in range(len(obj_ids)):
            pos = np.where(masks[i])
            xmin = np.min(pos[1])
            xmax = np.max(pos[1])
            ymin = np.min(pos[0])
            ymax = np.max(pos[0])
            boxes.append([xmin, ymin, xmax, ymax])

        boxes = torch.as_tensor(boxes, dtype=torch.float32)

        labels = [i if i < 1000 else int(i / 1000) for i in obj_ids]

        labels = torch.as_tensor(labels, dtype=torch.int64)
        masks = torch.as_tensor(masks, dtype=torch.uint8)

        # if id:
        #    id = torch.tensor([id])

        area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        iscrowd = torch.zeros((len(obj_ids),), dtype=torch.int64)

        target = {}
        target["boxes"] = boxes
        target["labels"] = labels
        target["masks"] = masks

        # if id:
        #    target["image_id"] = id

        target["area"] = area
        target["iscrowd"] = iscrowd

        return image, target


def get_transform(train):
    transforms = []
    transforms.append(ResizeData(256, 512))
    transforms.append(SimplifyAnnotation())
    transforms.append(AnnotationToMaskRCN())
    transforms.append(ToTensor())
    if train:
        transforms.append(RandomHorizontalFlip(0.5))
    return Compose(transforms)


def _flip_coco_person_keypoints(kps, width):
    flip_inds = [0, 2, 1, 4, 3, 6, 5, 8, 7, 10, 9, 12, 11, 14, 13, 16, 15]
    flipped_data = kps[:, flip_inds]
    flipped_data[..., 0] = width - flipped_data[..., 0]
    # Maintain COCO convention that if visibility == 0, then x, y = 0
    inds = flipped_data[..., 2] == 0
    flipped_data[inds] = 0
    return flipped_data


class Compose(object):
    def __init__(self, transforms):
        self.transforms = transforms

    def __call__(self, image, target):
        for t in self.transforms:
            image, target = t(image, target)
        return image, target


class RandomHorizontalFlip(object):
    def __init__(self, prob):
        self.prob = prob

    def __call__(self, image, target):
        if random.random() < self.prob:
            height, width = image.shape[-2:]
            image = image.flip(-1)
            bbox = target["boxes"]
            bbox[:, [0, 2]] = width - bbox[:, [2, 0]]
            target["boxes"] = bbox
            if "masks" in target:
                target["masks"] = target["masks"].flip(-1)
            if "keypoints" in target:
                keypoints = target["keypoints"]
                keypoints = _flip_coco_person_keypoints(keypoints, width)
                target["keypoints"] = keypoints
        return image, target


class ToTensor(object):
    def __call__(self, image, target):
        image = F.to_tensor(image)
        return image, target


class ResizeData:

    def __init__(self, height, width):
        self.height = height
        self.width = width

    def __call__(self, image, taret):
        res = Resize((self.height, self.width))
        return res.forward(image), res.forward(taret)
